import random as r

win, loss, tie = 0, 0, 0
rps = {"r":"ROCK", "p":"PAPER", "s":"SCISSORS"}
defeats = {"ROCK":"SCISSORS", "SCISSORS":"PAPER", "PAPER":"ROCK"}

print("ROCK, PAPER, SCISSORS")

while True:
    print(f"{win} Wins, {loss} Losses, {tie} Ties")
    print()

    comp = rps.get(r.choice(["r", "p", "s"]))    

    print("Enter your move: (r)ock (p)aper (s)cissors or (q)uit")
    guess = input()

    if guess == "q":
        break
    else:
        player = rps.get(guess)

        print(player + " versus...")
        print(comp)

        if player == comp:
            print("It's a tie!")
            tie += 1
        elif defeats.get(player) == comp:
            print("You win!")
            win += 1
        else:
            print("You lose!")
            loss +=1                     

print("Thanks for playing.")


